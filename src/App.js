import React, { useEffect, useState } from "react";
import "./App.css";

import Navbar from "./layouts/Navbar";
import Main from "./layouts/Main";
import Footer from "./layouts/Footer";
import Bottom from "./layouts/Bottom";

function App() {
  return (
    <div className="App">
      <div className="container">
        <header>20% OFF SITEWIDE UNTILL 9/12</header>
        <Navbar />
        <Main />
        <Footer />
        <Bottom />
      </div>
    </div>
  );
}

export default App;
