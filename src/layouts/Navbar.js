import React from "react";
import "../App.css";
import { BiUser, BiSearch } from "react-icons/bi";
import { FaBars } from "react-icons/fa";
import { BsBag } from "react-icons/bs";

function Navbar() {
  return (
    <nav>
      <section>
        <FaBars id="menuIcon" />
      </section>
      <section id="logo">AGAIN/FASTER</section>
      <section id="navbar">
        <a>LOREM</a>
        <a>CONSECTETUR</a>
        <a>TEMPOR</a>
        <a>MAGNA</a>
        <a>REPREHENDERT</a>
      </section>
      <section id="topIcons">
        <BiSearch className="navIcon" />
        <BiUser className="navIcon" />
        <BsBag className="navBag" />
      </section>
    </nav>
  );
}

export default Navbar;
