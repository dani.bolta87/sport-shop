import React from "react";
import "../App.css";

import { BsArrowRight } from "react-icons/bs";
import { FaFacebookSquare, FaInstagram } from "react-icons/fa";
import { MdEmail } from "react-icons/md";

function Footer() {
  return (
    <footer>
      <section className="footerSection1">
        <h1> NEWSLETTER </h1>
        <div>
          <input type="text" placeholder="Enter your email" />
          <i>
            <BsArrowRight className="inputArrow" />
          </i>
        </div>
        <span>
          <FaFacebookSquare className="footerIcons" />
          <FaInstagram className="footerIcons" />
          <MdEmail className="footerIcons" />
        </span>
      </section>
      <section className="footerSection2">
        <h1> AGAIN/FASTER</h1>

        <a>About Us</a>
        <a>Blog</a>
        <a>Gift Cards</a>
        <a>Millitary/LEO Discount</a>
        <a>Financing</a>
        <a>Retail Store</a>
      </section>

      <section className="footerSection3">
        <h1>GET HELP</h1>
        <a>Introductions</a>
        <a>Shipping</a>
        <a>Returns and Warranty</a>
        <a>Contact US</a>
        <a>Shop in Store</a>
      </section>
    </footer>
  );
}

export default Footer;
