import React from "react";
import "../App.css";
import Slider from "../Components/Slider";

function Main() {
  return (
    <main>
      <section id="section1">
        <section id="fistDetail" className="innerSection">
          <h1>Rubber Hex Dumbbells</h1>
          <section>
            Again Faster® Rubber Hex Dumbbells offer one of the most effective
            ways to add variance to any training program. Functional dumbbell
            movements cultivate stability, coordination and strength in ways
            that cannot be achieved using a barbell. Lighter dumbbells are great
            tools for accommodating athletes with an injury or those not yet
            comfortable with a bar. Whether you are making your first equipment
            purchase or looking to expand your gym's capabilities, making
            dumbbells a priority will take your training to the next level.
            <br />
            <br />
            <br />
            Need a way to organize your weights? Check out some of our Storage
            Options.
          </section>
        </section>
        <section className="innerSection">
          <figure id="firstImage">
            <img
              style={{
                width: "100%",
                height: "auto",
                resize: "contain",
              }}
              src={require("../images/dumbbells_40lb.png").default}
              alt="Dumbbell 40LB"
            />
          </figure>
        </section>
      </section>
      <section id="section2">
        <section className="innerSection">
          <figure id="secondImage">
            <img
              style={{
                width: "100%",
                height: "auto",
                resize: "contain",
              }}
              src={require("../images/dumbbells_instruction.png").default}
              alt="Dumbbell Instruction"
            />
          </figure>
        </section>
        <section className="innerSection">
          <article id="secondDetail">
            <ul>
              <li>Here’s what you need to know about our dumbbells</li>
              <li>
                <mark>/ </mark> Texture design for better grip during strength
                and endurance work work
              </li>
              <li>
                <mark>/ </mark> Lorem ipsum dolor sit amet, consectetur
                adipiscing elit work
              </li>
              <li>
                <mark>/ </mark> Sed ut perspiciatis unde omnis iste natus error
                sit voluptatem accusantium doloremque laudantium, totam rem
                aperiam
              </li>
            </ul>
          </article>
        </section>
      </section>
      <section id="section3">
        <section id="thirdDetail" className="innerSection">
          <h1>Perfect for indoor and outdoor workouts </h1>
        </section>
        <section className="innerSection">
          <figure id="thirdImage">
            <img
              style={{
                width: "100%",
                height: "auto",
                resize: "contain",
              }}
              src={require("../images/indoor_outdoor_workout.png").default}
              alt="Outdoor Workout Dumbbell"
            />
          </figure>
        </section>
      </section>
      <section id="section4">
        <h1>YOU MIGHT ALSO LIKE</h1>
        <section style={{ position: "relative" }} id="cardContainer">
          <Slider />
        </section>
      </section>
    </main>
  );
}

export default Main;
