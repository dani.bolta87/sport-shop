import React, { useEffect, useState } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import Card from "./card";
import "../App.css";
import { MdArrowBackIosNew, MdArrowForwardIos } from "react-icons/md";
function Slider() {
  const [next, setNext] = useState(0);
  const [slicerIndicator, setSlicerIndicator] = useState(4);

  const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
      paritialVisibilityGutter: 60,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 4,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 2,
    },
  };

  const products = [
    {
      id: "1",
      productName: "Product name",
      starRating: 5,
      price: 34,
      promotion: "Free Shipping",
      photoUrl: "gym_bench.png",
    },
    {
      id: "2",
      productName: "Product name",
      starRating: 5,
      price: 34,
      promotion: "Free Shipping",
      photoUrl: "fitness_ball.png",
    },
    {
      id: "3",
      productName: "Product name",
      starRating: 5,
      price: 34,
      promotion: "Free Shipping",
      photoUrl: "gym_ring.png",
    },
    {
      id: "4",
      productName: "Product name",
      starRating: 5,
      price: 34,
      promotion: "Free Shipping",
      photoUrl: "fitness_air_bike.png",
    },
    {
      id: "5",
      productName: "Product name",
      starRating: 5,
      price: 34,
      promotion: "Free Shipping",
      photoUrl: "gym_bench.png",
    },
    {
      id: "6",
      productName: "Product name",
      starRating: 5,
      price: 34,
      promotion: "Free Shipping",
      photoUrl: "fitness_ball.png",
    },
    {
      id: "7",
      productName: "Product name",
      starRating: 5,
      price: 34,
      promotion: "Free Shipping",
      photoUrl: "gym_bench.png",
    },
    {
      id: "8",
      productName: "Product name",
      starRating: 5,
      price: 34,
      promotion: "Free Shipping",
      photoUrl: "fitness_ball.png",
    },
  ];

  const sliderFunction = (isForward) => {
    if (isForward) {
      setNext(next + 1);
    } else {
      setNext(next - 1);
    }
  };

  return (
    <>
      <Carousel responsive={responsive} id="desktop">
        {products.slice(next, next + 4).map((product) => {
          return (
            <Card
              key={product.id}
              promotion={product.promotion}
              productName={product.productName}
              price={product.price}
              url={product.photoUrl}
            />
          );
        })}
      </Carousel>
      )
      {next && (
        <a
          onClick={() => sliderFunction(false)}
          style={{
            zIndex: 100,
            position: "absolute",
            color: "#ffffff",
            fontSize: 24,
            fontWeight: "bold",
            left: 30,
            top: "50%",
            backgroundColor: "black",
            opacity: 0.5,
            borderRadius: "30%",
          }}
        >
          <MdArrowBackIosNew />
        </a>
      )}
      {products.length - 4 !== next && (
        <a
          onClick={() => sliderFunction(true)}
          style={{
            zIndex: 100,
            position: "absolute",
            color: "#ffffff",
            fontSize: 24,
            fontWeight: "bold",
            right: 30,
            top: "50%",
            backgroundColor: "black",
            opacity: 0.5,
            borderRadius: "30%",
          }}
        >
          <MdArrowForwardIos />
        </a>
      )}
    </>
  );
}

export default Slider;
