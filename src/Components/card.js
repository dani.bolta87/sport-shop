import React from "react";
import "../App.css";

const Card = (props) => {
  const { promotion, productName, price, url } = props;
  console.log("promotion", url);

  const fiveStars = (
    <mark style={{ color: "#C2A578", backgroundColor: "transparent" }}>
      &#9733; &#9733; &#9733;&#9733; &#9733;
    </mark>
  );
  return (
    <div className="card">
      <div className="promotionTitle">{promotion}</div>
      <img
        className="cardProductImg"
        src={require(`./../images/${url}`).default}
        alt="bench"
        loading="lazy"
      />
      <section className="productDetail">
        <span>{productName}</span>
        {fiveStars}
        <span>
          &#36;
          {price}
        </span>
      </section>
    </div>
  );
};

export default Card;
